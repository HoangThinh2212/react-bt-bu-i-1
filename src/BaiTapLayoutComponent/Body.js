import React, { Component } from "react";
import Banner from "./Banner";
import Item from "./Item";
import Item2 from "./Item2";
import Item3 from "./Item3";
import Item4 from "./Item4";
import Item5 from "./Item5";
import Item6 from "./Item6";

export default class Body extends Component {
  render() {
    return (
      <div>
        <Banner />
        <div className="container">
          <div style={{ gap: "3.4rem", marginTop: "120px" }} className="row">
            <Item />
            <Item2 />
            <Item3 />
            <Item4 />
            <Item5 />
            <Item6 />
          </div>
        </div>
      </div>
    );
  }
}
