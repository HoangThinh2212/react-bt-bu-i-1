import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div className="container bg-light rounded h-100 px-5">
        <div className="p-5">
          <h3 className="mt-5 display-4 font-weight-bold">A warm welcome!</h3>
          <p className="display-5 h5 font-weight-normal">
            Bootstrap utility classes are used to create this jumbotron since
            the old component has been removed from the framework. Why create
            custom CSS when you can use utilities?
          </p>
          <a style= {{padding:"8px", width:"21%",fontSize:"22px"}}className="btn btn-primary btn-xlg text-light mb-5 mb-md-5 mt-2" href="#">
            Call to action
          </a>
        </div>
      </div>
    );
  }
}
