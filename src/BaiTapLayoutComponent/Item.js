import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div
        style={{ width: "47%", marginTop: "-50px" }}
        className="bg-light mb-5 p-5"
      >
        <div
          style={{ width: "4rem", height: "4rem", marginTop: "-75px" }}
          className="d-flex rounded-lg justify-content-center align-items-center button-outside bg-primary bg-gradient text-white mx-auto mb-3"
        >
          <i style={{ fontSize: "2rem" }} class="fa fa-layer-group"></i>
        </div>
        <div className="layout container">
          <h3>Fresh new layout</h3>
          <p>
            With Bootstrap 5, we've created a fresh new layout for this
            template!
          </p>
        </div>
      </div>
    );
  }
}
