import './App.css';
import Main from './BaiTapLayoutComponent/Main';

function App() {
  return (
    <div className="App">
      <Main/>
    </div>
  );
}

export default App;
